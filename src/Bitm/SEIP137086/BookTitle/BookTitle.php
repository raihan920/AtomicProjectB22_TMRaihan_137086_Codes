<?php
//namespace src\Bitm\SEIP137086\BookTitle;   for general type
namespace App\Bitm\SEIP137086\BookTitle; //using composer

class BookTitle{
    public $id = "";
    public $title = "";
    public $created = "";
    public $modified = "";
    public $created_by = "";
    public $modified_by = "";
    public $deleted_at = "";

    public function index(){
        echo "index - I am listing data";
    }
    public function create(){
        echo "create - I am creating data";
    }
    public function store(){
        echo "store - I am storing data";
    }
    public function edit(){
        echo "editing - I am editing data";
    }
    public function update(){
        echo "update - I am updating data";
    }
    public function delete(){
        echo "delete - I am deleting data";
    }
    public function view(){
        echo "view - I am viewing data";
    }
}
?>